# Specyfikacja

Urządzenie jest uniwersalną płytką uruchomieniową do MCU ATSAMC21E17A-MU (https://www.microchip.com/en-us/product/ATSAMC21E17A).

# Projekt PCB

Schemat: [doc/ATSAMC21E_EVB_V1_0_SCH.pdf](doc/ATSAMC21E_EVB_V1_0_SCH.pdf)

Widok 3D: [doc/ATSAMC21E_EVB_V1_0_3D.pdf](doc/ATSAMC21E_EVB_V1_0_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

# Licencja

MIT
